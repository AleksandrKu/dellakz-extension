'use strict';

let result_array = [];
let orders = document.querySelectorAll("#requests_all > div:nth-child(3) > div > div > table");

for (let i = 0; i < orders.length - 1; i++) {
    let order = {};
    let range_date_html = orders[i].querySelector("tbody > tr:nth-child(1) > td.country > nobr");
    let range_date;
    if (range_date_html) {
        range_date = orders[i].querySelector("tbody > tr:nth-child(1) > td.country > nobr").innerHTML;
    } else {
        range_date = orders[i].querySelector("tbody > tr:nth-child(1) > td.country").innerText;
    }

    let dates = range_date.split("—");
    order.date_start = dates[0];
    order.date_end = (dates[1]) ? dates[1] : dates[0];

   // let from_to_text = orders[i].querySelector("tbody > tr:nth-child(1)  > td:nth-child(2) > a.request_distance").innerText;
    let from_to_text = orders[i].querySelector("tbody > tr:nth-child(1)  > td:nth-child(2) > .request_distance").innerText;
    let countries = from_to_text.match(/(\w){2,}/gi);
    if (countries.length > 2) {
        order.from_to_text = from_to_text;
    } else {
        order.from_to_text = "";
        order.from_to_text_short = from_to_text;
    }
    order.from_city = orders[i].querySelector("tbody > tr:nth-child(1)  > td:nth-child(2) > .request_distance > b:nth-child(1)").innerText;
    order.from_region = orders[i].querySelector("tbody > tr:nth-child(1)  > td:nth-child(2) > .request_distance > b:nth-child(1)").getAttribute("title");
    order.from_country = countries[0];

    order.to_city = orders[i].querySelector("tbody > tr:nth-child(1)  > td:nth-child(2) > .request_distance > b:nth-last-child(1)").innerText;
    order.to_region = orders[i].querySelector("tbody > tr:nth-child(1)  > td:nth-child(2) > .request_distance > b:nth-last-child(1)").getAttribute("title");
    order.to_country = countries[countries.length - 1];

    order.weight = orders[i].querySelector("tbody > tr:nth-child(1) > td:nth-child(4) > b").innerText;
    order.volume = orders[i].querySelector("tbody > tr:nth-child(1) > td:nth-child(5) > b").innerText;

    let type_cargo = orders[i].querySelector("tbody > tr:nth-child(2) > td.text_gray_m > b").innerText;
    order.type_cargo = type_cargo.trim();

    let machines = orders[i].querySelector("tbody > tr:nth-child(2) > td.text_gray_m").innerText;
    order.machines = machines.replace(type_cargo, '').trim();

    let truck = orders[i].querySelector("tbody > tr:nth-child(1) > td:nth-child(3) > b").innerText;
    order.truck = truck;

    order.phone = orders[i].querySelector("tbody > tr:nth-child(3) > td.text_gray_m > span > span").innerText;
    order.name = orders[i].querySelector("tbody > tr:nth-child(3) > td:nth-child(2) > span").innerText;
    order.comment = orders[i].querySelector("tbody > tr:nth-child(1) > td:nth-child(6)").innerText;
    order.price = orders[i].querySelector("tbody > tr:nth-child(2) > td:nth-child(4)").innerText;

    order.price_add = orders[i].querySelector("tbody > tr:nth-child(1) > td:nth-child(6)").innerText;

    result_array.push(order);
    order = null;
}

chrome.storage.sync.set({orders_array: result_array}, function () {
});
